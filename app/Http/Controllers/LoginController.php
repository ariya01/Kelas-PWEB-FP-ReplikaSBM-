<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Pins;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Redirect;
use Response;
class LoginController extends Controller
{

    public function buka()
    {
    	return view('login');
    }

    public function buka2()
    {
    	return view('daftar');
    }

    public function add(Request $request)
    {
    	$hasil ='';
        for ($i=0;$i<7;$i++)
        {
            $hasil .=mt_rand(0,9);
        }
        $put ='';
        for ($i=0;$i<7;$i++)
        {
            $put .=mt_rand(0,9);
        }

        $pin ='';
        for ($i=0;$i<7;$i++)
        {
            $pin .=mt_rand(0,9);
        }
        // DB::table('masuks')->insert(
        //     ['name' => $request->nama,'kode' => $hasil ,'lahir' =>$request->lahir]
        //     );
        // Session::put('harta',$hasil);
        // return redirect('/bayar');
        $vars = [
        "per" => $hasil,
        "dua" => $put,
        "tiga"=> $pin];

        session()->put('blog', $vars);
        $data=Input::except(array('token'));
        // var_dump($data);
        $rule=array(
          'nama'=>'required' ,
          'lahir'=>'required' );

        $validator=Validator::make($data,$rule);

        if($validator->fails())
        {
            // echo "yes";
            return redirect('/daftar')->withErrors($validator);
        }
        else
        {
        //     DB::table('masuks')->insert(
        // ['name' => $request->nama,'kode' => $hasil ,'lahir' =>$request->lahir]
        // );
        //     return redirect('/bayar');
            Pins::formstore(Input::except(array('_token')));
            return redirect('/bayar');
        }
    }

    public function bayar(Request $request)
    {
        // $hasil ='';
        // for ($i=0;$i<7;$i++)
        // {
        //     $hasil .=mt_rand(0,9);
        // }

        // $pin ='';
        // for ($i=0;$i<7;$i++)
        // {
        //     $pin .=mt_rand(0,9);
        // }
        // $harta=Session::get('harta');
        // $hasil=session('blog.per');
        // DB::table('pins')->insert(
        //     ['kode'=>$harta
        //  ,'KAP'=>$hasil,'PIN'=>$pin,'status'=>0
        //     ]);
        $hasil =session('blog.per');
      $user = DB::table('pins')->where('email','=',$hasil)->first();
      // dd($user);



       return view ('/bayar',compact('user'));
   } 

    public function masuk()
    {

                // $request->session()->flush();
                // $credentials=array('name'=>$input['KAP'],'password'=>$input['pin']);
                // // dd($credentials);

                // if (Auth::attempt($credentials))
                // {
                //     return redirect('/bayar');
                // }
                // else
                // {
                //     return redirect('/bajing');
                // }
            // dd($request->KAP,$request->pin)
            // if(Auth::attempt(['email' => '$request->KAP', 'password' => '$request->pin'])){
            //    return redirect('/bajing');
            // }
            // else
            // {
            //     echo "string";
            // }
           $data=Input::except(array('_token'));
           $rule=array(
              'KAP'=>'required' ,
              'pin'=>'required' );

           $validator=Validator::make($data,$rule);

        if($validator->fails())
            {
                // echo "yes";
            return redirect('/login')->withErrors($validator);
            }
        else
            {
                // var_dump($data);
                // return redirect('/bayar');
                // $userdata=array(
                //     'email'=>Input::get('KAP'),
                //     'password'=>Input::get("pin")
                //     );
                // dd($userdata);
                // // dd($userdata);
                // if (Auth::check($userdata)){
                //      // return redirect('/masuk');
                //      echo "masuk";   
                // }
                // else
                // {
                //      // return redirect('/login');
                //     echo "belum";
                // }
            $user = Input::get('KAP');
            $pass = Input::get('pin');
            
            $admin = Pins::where('email', $user)->first();
            
            if ($admin!=null)
            {
                if($admin->password == $pass)
                {
                    if ($admin->status==1)
                    {

                        $temp = Auth::loginUsingId($admin->id);
                        //echo "hai sayang";
                        // dd(Auth::check());
                        return Redirect::to('/pilih');
                    }
                    else
                    {
                        echo "bayar dulu sayang";
                    }
                }
                else
                {
                    echo "salah username atau pasword";
                    // return Redirect::to('/login');
                }   
            }
            else
            {
                echo "salah username atau pasword";
            }

        }
            // echo "string";
    }

    public function keluar()
    {
        Auth::logout();
        return redirect('/');
    }

    public function pilih()
    {
             // $id = Auth::user()->getId();
             // dd($id);
            // dd(Auth::check());
        if (Auth::check())
        {
                // echo Auth::user()->nama;
            return view ('/pilih');
        }
        else
        {
            return redirect ('/');
        }
    }

    public function photo(Request $request)
    {
            // echo "hai sayang";
        if (Auth::check())
        {
                //dd($request->photo);
                // echo $request->hasFile('photo');
            if ($request->hasFile('photo'))
            {
                    // echo "string";
                $user=Auth::user()->id;
                $extension = Input::file('photo')->getClientOriginalExtension();
                    // dd($extension);
                $file="photo_".$user.'.'.$extension;
                $filefoto=$request->file('photo');
                $filefoto->move('foto/',$file);
                $email=Auth::user()->email;
                DB::table('photos')->insert(
                    ['email'=>$email,'photo'=>$file
                    ]);

                // return back ()->with('massage','Foto sudah Masuk');
                return redirect('/isi');
            }
            else
            {
                echo "hah";
            }
        }

    }
    
    public function display()
    {
        $email=Auth::user()->email;
        $users = DB::table('photos')
                     ->where('email', '=', $email)
                     ->first();
        // echo $users->photo;
        $path=$users->photo;
       $hasil='foto/'.$path;

       $prov = DB::table('provinsis')->get();
       // dd($prov);
       $kot = DB::table('kotas')->get();
       // dd($kot);
        return view('/isi',compact('hasil'),compact('prov'));
    }

    public function jax($cat_id)
    {
        $cat=$cat_id;
        $subcategories = DB::table('kotas')->where('kodeprov','=',$cat)->get();
        return Response::json($subcategories);
    }

    public function biodata(Request $request)
    {
        $nama= Auth::user()->nama;
        $lahir = Auth::user()->lahir;
        $email =Auth::user()->email;

        // dd($request->optradio);

        $bio=DB::table('bios')->insert([
            'nama' => $nama,
            'email' => $email,
            'input' => $request->input,
            'induk'=>$request->induk,
            'alamat'=> $request->alamat,
            'kodeprov' => $request->prov,
            'kodekot' => $request->subcategory,
            'pos'=> $request->pos,
            'hp' => $request->hp,
            'surat' => $request->surat, 
            'kodesurat' => $request->suratcek, 
            'kodeprovla' => $request->provinsila,
            'kodekotla' => $request->subcategoryla, 
            'lahir' => $lahir,
            'jenis' => $request->optradio,
            'agama' => $request->agama , 
            'warga' => $request->warga,
            'jumadik'=> $request->jumadik,
            'jumkakak' =>$request->jumkakak,
            'namaayah'=>$request->namaayah,
            'namaibu' =>$request->namaibu,
            'penayah'=> $request->penayah,
            'penibu'=> $request->penibu,
            'pekayah'=>$request->pekayah,
            'pekibu'=>$request->pekibu,
            'hasayah'=> $request->hasayah,
            'hasibu'=>$request->hasibu,
            'but'=>$request->but
            ]);

        return redirect ('/jurusan');
    }
    public function jurusan()
    {
        $email=Auth::user()->email;
        $users = DB::table('photos')
                     ->where('email', '=', $email)
                     ->first();
        // echo $users->photo;
        $path=$users->photo;
       $hasil='foto/'.$path;

       $panlok = DB::table('panloks')->get();
       $univ = DB::table('univs')->get();
       return view('jurusan',compact('hasil','panlok','univ'));
    }
    public function jaxnya($cat_id)
    {
        $cat=$cat_id;
        $subcategories = DB::table('subpanlokss')->where('kodepanlok','=',$cat)->get();
        return Response::json($subcategories);
    }

     public function jaxmu($cat_id)
    {
        $cat=$cat_id;
        $subcategories = DB::table('jurs')->where('kodeuniv','=',$cat)->get();
        return Response::json($subcategories);
    }

    public function ter(Request $request)
    {
        $email = Auth::user()->email;
        $masuk = DB::table('pilihs')->insert([
            'panlok' => $request->panlok,
            'subpanlok' => $request->subpanlok,
            'univ1' => $request->univ1,
            'univ2' => $request->univ2,
            'univ3' => $request->univ3,
            'jur1' => $request->jur1,
            'jur2' => $request->jur2,
            'jur3' => $request->jur3,
            'olah' => $request->olah,
            'seni' => $request->seni,
            'sendra' => $request->sendra,
            'email' => $email,
            ]);

        return redirect('/ipc');
    }

    public function cetak ()
    {
        $user = Auth::user()->email;
        $data = DB::table('bios')->where('email','=',$user)->first();
        $data1 = DB::table('cbts')->where('email','=',$user)->first();
        $email=Auth::user()->email;
        $users = DB::table('photos')
                     ->where('email', '=', $email)
                     ->first();
        // echo $users->photo;
        $path=$users->photo;
       $hasil='foto/'.$path;
        // dd($data);
       $univ1 = DB::table('pilihs')->leftJoin('panloks','pilihs.panlok','=','panloks.kodepanlok')
       ->Join('subpanlokss','pilihs.subpanlok','=','subpanlokss.kodesub')
       ->Join('univs','pilihs.univ1','=','univs.kodeuniv')
       ->where('email','=',$user)->first();
       // dd($univ1);
        $univ2 = DB::table('pilihs')->leftJoin('univs','pilihs.univ2','=','univs.kodeuniv')
        ->where('email','=',$user)->first();
        $univ3 = DB::table('pilihs')->leftJoin('univs','pilihs.univ3','=','univs.kodeuniv')
        ->where('email','=',$user)->first();
        $jur1 = DB::table('pilihs')->leftJoin('jurs','pilihs.jur1','=','jurs.kodejur')
        ->where('email','=',$user)->first();
       //dd($jur2);
       $jur2 = DB::table('pilihs')->leftJoin('jurs','pilihs.jur2','=','jurs.kodejur')
        ->where('email','=',$user)->first();
        $jur3 = DB::table('pilihs')->leftJoin('jurs','pilihs.jur3','=','jurs.kodejur')
        ->where('email','=',$user)->first();
       // $pilih = DB::table('pilihs')->leftJoin('cbts','pilih.email','=','cbts.email')
       //  ->first();
       
        // dd($univ1);
        return view ('/cetak',compact('data','data1','hasil','univ1','univ2','univ3','jur1','jur2','jur3'));
    }

    public function ipc()
    {
        return view('/ipc');   
    }

    public function cbt(Request $request)
    {
         $email = Auth::user()->email;
         $masuk = DB::table('cbts')->insert(
            ['email' => $email,
            'cara' => $request->cara,
            'rumpun' => $request->rumpun,
            'tahun' => $request->tahun,
            ]);
         return redirect('/cetak');
     
    }

    public function absen ()
    {
        $user = DB::table('pins')->leftJoin('photos','pins.email','=','photos.email')->get();
         // dd($user);
        return view('/absen',compact('user'));
    }
}
