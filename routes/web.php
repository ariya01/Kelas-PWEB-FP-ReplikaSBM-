<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login','LoginController@buka');
Route::get('/daftar','LoginController@buka2');
Route::post('/insert','LoginController@add');
Route::get('/bayar','LoginController@bayar');
Route::post('/coba','LoginController@masuk');
Route::get('/pilih','LoginController@pilih');
Route::post('/masuk_photo','LoginController@photo');
Route::post('/keluar_kok','LoginController@keluar');
Route::get('/isi','LoginController@display');
Route::get('/ajaxku/{cat_id}','LoginController@jax');
Route::post('/bio','LoginController@biodata');
Route::get('/jurusan','LoginController@jurusan');
Route::get('/ajaxnya/{cat_id}','LoginController@jaxnya');
Route::get('/ajaxlain/{cat_id}','LoginController@jaxmu');
Route::post('/pilih_jur','LoginController@ter');
Route::get('/cetak','LoginController@cetak');
Route::get('/ipc','LoginController@ipc');
Route::post('/cbt','LoginController@cbt');
Route::get('/absen','LoginController@absen');