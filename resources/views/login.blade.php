@extends('layout.app')

@section('title')
Pendaftaran SBMPTN
@endsection

@section('css')
            td.ty1 {width:100%;background-color:#e7efff;}
            td.c1 {width:160px;height:30px;vertical-align:middle;font:bold 12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
            td.c2 {width:10px;height:30px;vertical-align:middle;font:12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
@endsection

@section('content')
</table>                </td>
            </tr>
            <tr>
                <td class="tc">
                    <p class="sp">&nbsp;</p>
                    <p class="ti1">LOGIN PENDAFTARAN ONLINE SBMPTN 2017</p>
                    <p class="sp">&nbsp;</p>
                    <form action="/coba" method="post">
                     <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <table class="ct">
                            <tr> 
                                <td class="ty1">
                                    <table width="100%">
                                        <tr>
                                          <td class="c1">Kode Akses Pendaftaran (KAP)</td>
                                          <td class="c2">:</td>
                                          <td style="text-align:left">
                                            <input name="KAP" type="text" />
                                             @if($errors->has('KAP'))
                                            <p style="color:red;">KAP belum ada</p>
                                            @endif
                                          </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr> 
                                <td class="ty1">
                                    <table width="100%">
                                        <tr>
                                          <td class="c1">PIN</td>
                                          <td class="c2">:</td>
                                          <td style="text-align:left">
                                            <input name="pin" type="password")/>
                                             @if($errors->has('pin'))
                                            <p style="color:red;">Pin belum ada</p>
                                            @endif
                                          </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                            
                           <tr>
                                <td class="btcont" colspan="2">
                                    <input class="btn btn-primary" type="submit" value="LOGIN" />
                                </td>
                            </tr> 
                        </table>
                    </form>
                    <br /><br />
                    
                </td>
            </tr>
        </table>
@endsection