<!DOCTYPE html>
<html>
<head>
	<title>
		
	</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
</head>
<body>
	<div class="container">
		<div class="row" style="border: 1px solid black; padding: 10px">
			<div class="col-md-3" style="width: 200px;">
				<img src="{{ asset('img/logosbmptn.gif') }}">
			</div>
			<div class="col-md-4" style="padding-top: 10px">
				<p style="font-size: 18px">BUKTI HADIR PESERTA SBMPTN 2017</p>
				<table>
					<tr>
						<td>SECTOR</td>
						<td style="padding-left: 10px">:</td>
						<td>JAWA TIMUR</td>
					</tr>
					<tr>
						<td>LOKASI</td>
						<td style="padding-left: 10px">:</td>
						<td>SMAN 1 JOGJAKARTA</td>
					</tr>
					<tr>
						<td>RUANG</td>
						<td style="padding-left: 10px">:</td>
						<td>TT - 900</td>
					</tr>
					<tr>
						<td>KELOMPOK</td>
						<td style="padding-left: 10px">:</td>
						<td>505HJM</td>
					</tr>
				</table>
			</div>
		</div>
		<?php $no=1;?>
				@foreach($user as $a)
		<div class="row" style="border: 1px solid black; margin-top: 10px">
			
			<div class="col-md-4" style="width: 180px">
				<img style="width: 150px; padding: 10px" src="{{ asset('foto/'.$a->photo) }}">
			</div>
			<div class="col-md-4" style="text-align: left; width: 500px; padding-top: 20px">
				
				<table style="margin-top: 40px">
					<tr>
						<td>NOMOR URUT</td>
						<td style="padding-left: 10px"> :</td>
						<td>{{$no++}}</td>
					</tr>
					<tr>
						<td>NOMOR PESERTA</td>
						<td style="padding-left: 10px"> :</td>
						<td>{{$a->KAP}}</td>
					</tr>
					<tr>
						<td>NAMA PESERTA</td>
						<td style="padding-left: 10px"> :</td>
						<td>{{$a->nama}}</td>
					</tr>
					<tr>
						<td>KEBUTUHAN KHUSUS</td>
						<td style="padding-left: 10px"> :</td>
						<td>-</td>
					</tr>
					<tr>
						<td>NAMA DI LJU</td>
						<td style="padding-left: 10px"> :</td>
						<td>{{$a->nama}}</td>
					</tr>
				</table>
			</div>
			<div class="col-md-4" style="margin-top: 50px">
				<table align="center" style="text-align:center; border: 1px solid black">
					<tr>
						<td style="padding: 70px 20px 0px 20px">TANDA TANGAN</td>
					</tr>
					<tr>
						<td style="padding: 0px 20px 10px 20px">{{$a->KAP}}</td>
					</tr>
				</table>
				
			</div>

		</div>
		@endforeach
	</div>
	
</body>
</html>