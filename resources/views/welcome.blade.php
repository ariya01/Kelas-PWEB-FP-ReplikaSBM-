<html dir="ltr" lang="id">
<head>
    <title>Pendaftaran Online SBMPTN 2017</title>
    <link rel="icon" href="./images/sbmptn.ico" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="31 Dec 2017 00:00:00 GMT" /> 
    <meta http-equiv="x-frame-options" content="deny" />
    <link rel="stylesheet" type="text/css" href="sbmptn.css" /> 
    <style type="text/css">
        body {margin:0;border:0;}
        a {text-decoration:none;color:#0000FF;font-weight:bold;}
        td.jud {text-align:center;vertical-align;middle;height:100%;}
        table.st1 {text-align:center;vertical-align:top;width:100%;height:100%;border:0;padding:0;border-spacing:0;}
        li.t1 {font: 14px Verdana,Geneva,Arial;color:#000000; line-height:22px;}
        li.t2 {font: 14px Verdana,Geneva,Arial;color:#ff0000; line-height:22px;}
        .tc {text-align:center;vertical-align:middle;height:100%;}
        .ct {border-spacing:5px;padding:4px;border:0;text-align:center;margin:auto;}
        .logo1 {text-align:left;vertical-align:middle;background-color:#2e72b6;height:100px;width:100px;}
        .logo2 {text-align:left;vertical-align:middle;background-color:#2e72b6;}
        .logo3 {text-align:right;vertical-align:middle;background-color:#ffffff;width:374px;}
        .nb {border:0;padding:0;border-spacing:0;}
        .f1 {font-family:Tahoma,Verdana,Arial,'Sans Serif';color:white;font-size:14px; }
        .f2 {font-family:Verdana,Geneva,Arial;font-size:50px;font-weight:bold; }
        .ti1 {font:bold 24px Tahoma,Verdana,Arial,'Sans Serif';color:#666666}
        .ti2 {font:16px Tahoma,Verdana,Arial,'Sans Serif';color:#ff871b;}
        .ti3 {font:14px Tahoma,Verdana,Arial,'Sans Serif';color:#bbbbbb;}
        .sp {font:4px Tahoma,Verdana,Arial,'Sans Serif'; color: #aaaaaa;}
        .ket {font:12px Tahoma,Verdana,Arial,'Sans Serif';color:#4a4a4a;}
        .att {font:14px Tahoma,Verdana,Arial,'Sans Serif';color:#888888;padding-top:2px;padding-bottom:2px;}
        .attsp {font:14px Tahoma,Verdana,Arial,'Sans Serif';color:#ff0000;padding-top:2px;padding-bottom:2px;}
        .attbd {font:14px Tahoma,Verdana,Arial,'Sans Serif';color:#000000;padding-top:2px;padding-bottom:2px;}
        .attti {font:22px Tahoma,Verdana,Arial,'Sans Serif';color:#ff871b;padding-top:2px;padding-bottom:2px;}
        .link {font: Bold 12px Tahoma,Verdana,Arial,'Sans Serif';}
        .err {text-align:center;font:14px Tahoma, Verdana, Arial, 'Sans Serif';color:#ff0000;}
        .bl {border-spacing:8px;padding:8px;border:0;width:100%;text-align:center;}
        .bd {border-spacing:4px;padding:4px;border:1;width:100%;text-align:center;}
        .tbd {border-color:#9391aa;border-spacing:4px;padding:4px;width:100%;border:0;}
        .tds {background-color:#e7efff;border-color:#ffffff;text-align:left;vertical-align:top;padding:5px;}
        .sj {font:22px Tahoma,Verdana,Arial,'Sans Serif';color:#83819f;text-align:left;}
        .sjsp {font:22px Tahoma,Verdana,Arial,'Sans Serif';color:#ff0000;text-align:left;}
        .sjk {font:18px Tahoma,Verdana,Arial,'Sans Serif';color:#83819f;text-align:left;}
        .sjksp {font:18px Tahoma,Verdana,Arial,'Sans Serif';color:#ff0000;text-align:left;}
        .sjkbd {font:18px Tahoma,Verdana,Arial,'Sans Serif';color:#000000;text-align:left;}
        .btcont {vertical-align:middle;text-align:center;height:50px;}
        .btgrey {
            border: medium none;background: url(./images/tombolbw.jpg) #fefefe no-repeat 0px 0px; 
            font: bold 14px/18px Geneva, Arial, Helvetica, sans-serif;width:104px;color:#ffffff;height:30px;
        }
        .btgreyw {
            border: medium none;background: url(./images/tombollbw.jpg) #fefefe no-repeat 0px 0px; 
            font: bold 14px/18px Geneva, Arial, Helvetica, sans-serif;width:140px;color:#ffffff;height:30px;
        }
        .lb {font:16px Tahoma,Verdana,Arial,'Sans Serif';color:#888888;padding-top:2px;padding-bottom:2px;}
        .lbsp {font:16px Tahoma,Verdana,Arial,'Sans Serif';color:#ff0000;padding-top:2px;padding-bottom:2px;}
        .lbbd {font:16px Tahoma,Verdana,Arial,'Sans Serif';color:#000000;padding-top:2px;padding-bottom:2px;}
        .dash {font:12px Tahoma,Verdana,Arial,'Sans Serif';}
        .captcha { display:table-cell; width:500px;height:100px;left:auto;right:auto;top:auto;bottom:auto;text-align:center;vertical-align:middle; }

        .marquee {height:30px;overflow:hidden;position:relative;background:#201059;color:yellow;border:0;padding:0;border-spacing:0;
            font-family:Tahoma,Verdana,Geneva,Arial;font-size:14px;white-space:nowrap;
        }
        .marquee span {width:100%;height:30px;line-height:30px;position:absolute;top:0;
            -webkit-animation: mymove 60s linear infinite; /* Chrome, Safari, Opera */ 
            animation: mymove 60s linear infinite;
        }
        /* Chrome, Safari, Opera */ 
        @-webkit-keyframes mymove { 0% {left: 110%; } 100% {left: -160%; } }
        /* Standard syntax */
        @keyframes mymove { 0% {left: 110%; } 100% {left: -160%;} }
        div.hd1 {border:5px solid #2e72b6;padding:3px;background-color:white;border-radius:10px;width:440px;height:200px;
            display:block; margin:10px 5px 10px 5px;
        }
        a.hd1:link,a.hd1:visited {border:5px outset #578ebe;padding:20px;background-color:#2e72b6;border-radius:10px;
            width:390;height:150px;
            color:white;font:normal 20px Tahoma,Geneva,sans-serif;text-align:center;vertical-align:middle;
            text-decoration:none;display:block;
        }
        a.hd1:hover { background-color:#23578b; }
        a.hd1:active { background-color:#23578b; border:5px inset #578ebe;}
        div.hd2 {border:5px solid #d77217;padding:3px;background-color:white;border-radius:10px;width:440px;height:200px;
            display:block; margin:10px 10px 10px 10px;
        }
        a.hd2:link,a.hd2:visited {border:5px outset #ff871b;padding:20px;background-color:#ff871b;border-radius:10px;
            width:390px;height:150px;
            color:white;font:normal 20px Tahoma,Geneva,sans-serif;text-align:center;vertical-align:middle;
            text-decoration:none;display:block;
        }
        a.hd2:hover { background-color:#d77217; }
        a.hd2:active { background-color:#d77217; border:5px inset #ff871b;}
        div.hd3 {border:5px solid #201059;padding:3px;background-color:white;border-radius:10px;width:400px;height:200px;
            display:block; margin:10px 10px 10px 10px;
        }
        a.hd3:link,a.hd3:visited {border:5px outset #17385a;padding:20px;background-color:#201059;border-radius:10px;
            width:350px;height:150px;
            color:white;font:normal 20px Tahoma,Geneva,sans-serif;text-align:center;vertical-align:middle;
            text-decoration:none;display:block;
        }
        span.hd {display: table-cell; vertical-align: middle; height:140px;}
        div.hd {display: table-cell; vertical-align: middle; height:140px;}
        a.hd3:hover { background-color:#4522bf; }
        a.hd3:active { background-color:#4522bf; border:5px inset #17385a;}
        a.hd4:link,a.hd4:visited {border:5px outset #17385a;padding:20px;background-color:#1a75ff;border-radius:10px;
                width:350px;height:150px;
                color:white;font:normal 20px Tahoma,Geneva,sans-serif;text-align:center;vertical-align:middle;
                text-decoration:none;display:block;
            }
            a.hd4:hover { background-color:#b3d1ff; }
            a.hd4:active { background-color:#b3d1ff; border:5px inset #17385a;}

            a.hd5:link,a.hd5:visited {border:5px outset #17385a;padding:20px;background-color:#b3b300;border-radius:10px;
                width:350px;height:150px;
                color:white;font:normal 20px Tahoma,Geneva,sans-serif;text-align:center;vertical-align:middle;
                text-decoration:none;display:block;
            }
            a.hd5:hover { background-color:#b3d1ff; }
            a.hd5:active { background-color:#b3d1ff; border:5px inset #17385a;}

    </style>
    <script type="text/javascript" charset="UTF-8">
        <!-- 
        if(!navigator.cookieEnabled) {
            var t="./wcookie.php";
            window.location.assign(t);
        } 
    -->
</script>
</head>
<body>
    <table class="st1">
        <tr>
            <td class="tc">

                <table class="st1">
                    <tr class="nb">
                        <td style="text-align:center;border:0;border-spacing:0;padding:0;" colspan="3">
                            <table class="st1">
                                <tr>
                                    <td class="logo1">
                                        <a href="#">
                                            <img src="{{asset('img/logosbmptn.gif')}}" alt="Logo SBMPTN" />
                                        </a>
                                    </td>
                                    <td class="logo2">
                                        <span class="f1">Laman Resmi</span><br />
                                        <span class="f2" style="color:white;">SBMPTN</span>
                                        <span class="f2" style="color:#ff871b;">2017</span><br />
                                        <span class="f1">
                                            Seleksi Bersama Masuk Perguruan Tinggi Negeri Tahun 2017
                                        </span>
                                    </td>
                                    <td class="logo3">
                                        <img style="vertical-align:middle" src="{{asset('img/bank.gif')}}" alt="Logo Bank Mitra" />
                                        <img style="vertical-align:middle" src="{{asset('img/telkom.gif')}}" alt="Logo Telkom" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="background-color:#bbbbbb;height:3px">
                        </td>
                    </tr>
                    <tr>
                        <td class="marquee">
                            <span>PANLOK 11 menyelenggarakan ujian keterampilan Olah Raga, Seni Rupa, Sendratasik, Seni Tari, dan Seni Musik di Universitas Syiah Kuala</span>
                        </td>
                    </tr>
                </table>                </td>
            </tr>
            <tr>
                <td class="tc">
                    <p class="sp">&nbsp;</p>
                    <p class="ti1">PENDAFTARAN ONLINE SBMPTN 2017</p>
                    <p class="ti1">                 </p>
                    <p class="sp">&nbsp;</p>
                    <p class="ti1" style="color:red;font-size:20px;">Pendaftaran Online SBMPTN 2017 telah berakhir!</p>
                    <table class="ct">
                        <tr> 
                            <td>
                                <div class="hd3">
                                    <a href="{{url('daftar')}}" class="hd4">
                                        <span class="hd">
                                            Bagi calon peserta SBMPTN 2017 yang belum memiliki pasangan KAP dan PIN serta <b>BUKAN</b> peserta Bidikmisi, silahkan di klik disini untuk mendapatkan KAP dan PIN
                                        </span>
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="hd3">
                                    <a href="login.php" class="hd5">
                                        <span class="hd">
                                            Bagi calon peserta SBMPTN 2017 yang belum memiliki pasangan KAP dan PIN serta <b>BUKAN</b> peserta Bidikmisi, silahkan di klik disini untuk mendapatkan KAP dan PIN
                                        </span>
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="hd3">
                                    <a href="{{url('login')}}" class="hd3">
                                        <span class="hd">
                                            Peserta SBMPTN 2017 yang sudah permanen silahkan klik di sini untuk Cetak Ulang Kartu Peserta.
                                        </span>
                                    </a>
                                </div>
                            </td>
                        </tr> 
                    </table>

                    <br /><br />
                    <table style="text-align:center">
                        <tr>
                            <td align="left">
                                <span class="attti"><strong>PERHATIAN :</strong></span>
                                <ul style="margin-top: 0px; margin-bottom: 0px; list-style-type:square">
                                    <li class="att">Apabila ada hal-hal yang kurang jelas tentang pendaftaran online ini, 
                                        dapat menghubungi Helpdesk SBMPTN 2017 di alamat <span class="attbd"><b>http://halo.sbmptn.ac.id</b></span> 
                                        atau nomor telepon <span class="attbd"><b>0804-1-456-456</b></span></li>
                                    </ul>
                                    <br />
                                    <img src="./images/digicert.jpg" alt="Logo Digicert" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        </html>