@extends('layout.app')

@section('title')
Pendaftaran SBMPTN
@endsection

@section('css')
td.ty1 {width:100%;background-color:#e7efff;}
td.c1 {width:160px;height:30px;vertical-align:middle;font:bold 12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
td.c2 {width:10px;height:30px;vertical-align:middle;font:12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
@endsection

@section('content')
</table>                
</td>
</tr>
<tr>
    <td class="tc">
        <p class="sp">&nbsp;</p>
        <p class="ti1">PENDAFTARAN KAP DAN PIN SBMPTN 2017</p>
        <p class="sp">&nbsp;</p>
        <div class="row">
        	<div class="col-md-7"><p class="ti1">Pengisian Biodata</p></div>
        	<div class="col-md-4"><img src="{{ asset($hasil) }}" height="130" width="130"></div>
        </div>
        <br>
        <form action="/pilih_jur" method="post">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="container">
            <div class="row">
                <div class="col-md-11 col-md-offset-0">
                    <div class="panel panel-default">
                        <div class="panel-heading ">
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Provinsi</p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="panlok" class="panlok" id="provinsi">
                                <option value="0" disabled="true" selected="true">-Select-</option>
                                @foreach ($panlok as $a)
                                <option value="{{$a->kodepanlok}}">{{$a->namapanlok}}</option>
                                @endforeach
                                </select> </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Kota</p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="subpanlok" id="subcategory"> 
                                <option value="0" disabled="true" selected="true">Pilih Panitia Lokal</option>
                                <option value=""></option>
                                </select> </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Pilihan 1</p></div>
                                <div class="col-md-8 text-left"> 
                                <select class="univ1" id="provinsila" name="univ1">
                                <option value="0" disabled="true" selected="true">-Select-</option>
                                @foreach ($univ as $a)
                                <option value="{{$a->kodeuniv}}">{{$a->namauniv}}</option>
                                @endforeach
                                </select> </div>
                            </div>
                             <div class="row ">
                                <div class="col-md-3"><p class="text-left"></p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="jur1" id="subcategoryla"> 
                                <option value="0" disabled="true" selected="true">-Pilih Program Studi-</option>
                                <option value=""></option>
                                </select> </div>
                            </div>
                            <br>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Pilihan 2</p></div>
                                <div class="col-md-8 text-left"> 
                                <select class="univ2" id="pil2" name="univ2">
                                <option value="0" disabled="true" selected="true">-Select-</option>
                                @foreach ($univ as $a)
                                <option value="{{$a->kodeuniv}}">{{$a->namauniv}}</option>
                                @endforeach
                                </select> </div>
                            </div>
                             <div class="row ">
                                <div class="col-md-3"><p class="text-left"></p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="jur2" id="pilku"> 
                                <option value="0" disabled="true" selected="true">-Pilih Program Studi-</option>
                                <option value=""></option>
                                </select> </div>
                            </div>
                        <br>
                        <div class="row ">
                                <div class="col-md-3"><p class="text-left">Pilihan 3</p></div>
                                <div class="col-md-8 text-left"> 
                                <select class="univ3" id="pil3" name="univ3">
                                <option value="0" disabled="true" selected="true">-Select-</option>
                                @foreach ($univ as $a)
                                <option value="{{$a->kodeuniv}}">{{$a->namauniv}}</option>
                                @endforeach
                                </select> </div>
                            </div>
                             <div class="row ">
                                <div class="col-md-3"><p class="text-left"></p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="jur3" id="pilku3"> 
                                <option value="0" disabled="true" selected="true">-Pilih Program Studi-</option>
                                <option value=""></option>
                                </select> </div>
                            </div>
                            <br>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">PTN tempat Anda mengikuti ujian Keterampilan</p></div>
                                <div class="col-md-8 text-left"> Ujian Keterampilan Olah raga:<br>
                                <select name="olah" id="pilku3"> 
                                <option value="0" disabled="true" selected="true">-Pilih Program Studi-</option>
                                <option>ITS</option>
                                <option>ITB</option>
                                <option>UNAIR</option>
                                </select> </div>
                            </div>
                             <div class="row ">
                                <div class="col-md-3"><p class="text-left"></p></div>
                                <div class="col-md-8 text-left"> Ujian Seni Rupa/Desain:<br>
                                <select name="seni" id="pilku3"> 
                                <option value="0" disabled="true" selected="true">-Pilih Program Studi-</option>
                                <option>ITS</option>
                                <option>ITB</option>
                                <option>UNAIR</option>
                                </select> </div>
                            </div>
                            <br>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left"></p></div>
                                <div class="col-md-8 text-left"> Ujian Sendratasik:<br>
                                <select name="sendra" id="pilku3"> 
                                <option value="0" disabled="true" selected="true">-Pilih Program Studi-</option>
                                <option>ITS</option>
                                <option>ITB</option>
                                <option>UNAIR</option>
                                </select> </div>
                            </div>
                     </div>
                     <br>
                      <div class="row ">
                                <div class="col-md-10"><p class="text-left"></p></div>
                                <div class="col-md-1 text-left">
                                <input class="btn btn-primary" type="submit" value="Simpan" /> 
                                </div>
                                </form>
                        </div>
                        <br>
                        <form action="/keluar_kok" method="post">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}"> 
                        <div class="row ">
                                <div class="col-md-10"><p class="text-left"></p></div>
                                <div class="col-md-1 text-left">
                                <input class="btn btn-primary" type="submit" value="Keluar" /> 
                                </div>
                        </div>
                        </form>
                        <br>
                </div>
            </div>
        
        </div>
    <br /><br />
</td>
</tr>
</table>
<<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
        $('#provinsi').on('change',function(e){
            // console.log(e);

            var cat_id =e.target.value;
            // console.log(cat_id);
        	$.get('/ajaxnya/'+cat_id, function(data){
        	// console.log(data);
        	$('#subcategory').empty();
        	$.each(data,function(index,subcat){
        		$('#subcategory').append('<option value="'+subcat.kodesub+'">'+subcat.namasub+'</option>');
        	});
        })
	});
        $('#provinsila').on('change',function(e){
            // console.log(e);

            var cat_id =e.target.value;
            // console.log(cat_id);
            $.get('/ajaxlain/'+cat_id, function(data){
            // console.log(data);
            $('#subcategoryla').empty();
            $.each(data,function(index,subcat){
                $('#subcategoryla').append('<option value="'+subcat.kodejur+'">'+subcat.namauniv+'</option>');
            });
        })
    });

 $('#pil2').on('change',function(e){
            // console.log(e);

            var cat_id =e.target.value;
            // console.log(cat_id);
            $.get('/ajaxlain/'+cat_id, function(data){
            // console.log(data);
            $('#pilku').empty();
            $.each(data,function(index,subcat){
                $('#pilku').append('<option value="'+subcat.kodejur+'">'+subcat.namauniv+'</option>');
            });
        })
    });

 $('#pil3').on('change',function(e){
            // console.log(e);

            var cat_id =e.target.value;
            // console.log(cat_id);
            $.get('/ajaxlain/'+cat_id, function(data){
            // console.log(data);
            $('#pilku3').empty();
            $.each(data,function(index,subcat){
                $('#pilku3').append('<option value="'+subcat.kodejur+'">'+subcat.namauniv+'</option>');
            });
        })
    });

</script>
@endsection