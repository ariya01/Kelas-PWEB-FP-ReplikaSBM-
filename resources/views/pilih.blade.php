@extends('layout.app')

@section('title')
Pendaftaran SBMPTN
@endsection

@section('css')
td.ty1 {width:100%;background-color:#e7efff;}
td.c1 {width:160px;height:30px;vertical-align:middle;font:bold 12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
td.c2 {width:10px;height:30px;vertical-align:middle;font:12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
@endsection

@section('content')
</table>                </td>
</tr>
<tr>
  <td class="tc">
    <p class="sp">&nbsp;</p>
    <p class="ti1">PENDAFTARAN SBMPTN 2017</p>
    <p class="sp">&nbsp;</p>
    <form action="/masuk_photo" method="post" enctype="multipart/form-data">
     <input type="hidden" name="_token" value="{!! csrf_token() !!}">
     <table class="ct">
      <tr> 
        <td class="ty1">
          <table width="100%">
            <tr>
              <td class="c1">Kode Akses Pendaftaran (KAP)</td>
              <td class="c2">:</td>
              <td style="text-align:left">
                <input name="photo" type="file" />
                @if($errors->has('photo'))
                <p style="color:red;">Belum Memasukan Photo</p>
                @endif
                @if(Session::has('massage'))
                <p>{{ session('massage') }}
                </p>
                @endif
                <td class="btcont" colspan="2">
                  <input class="btn" type="submit" value="Upload" />
                </td>
              </td>
            </tr>
          </table>
        </td>
      </tr>                           
      <tr>

      </tr> 
    </table>
  </form>
  <form action="/keluar_kok" method="post">
   <input type="hidden" name="_token" value="{!! csrf_token() !!}">
   <table class="ct">
    <tr> 
      <td class="">
        <table width="100%">
          <tr>
            <td class="c1"></td>
            <td class="c2"></td>
            <td style="text-align:right">
              <span class="glyphicon glyphicon-arrow-right btn-lg btn-block"></span>
              <td class="btcont " colspan="2">
                <input class="btn btn-primary " type="submit" value="Keluar" />
              </td>
            </td>
          </tr>
         </table>
      </td>
    </tr>                           
    <tr>

    </tr> 
  </table>
</form>
<br /><br />

</td>
</tr>
</table>
@endsection