@extends('layout.app')

@section('title')
Pendaftaran SBMPTN
@endsection

@section('css')
td.ty1 {width:100%;background-color:#e7efff;}
td.c1 {width:160px;height:30px;vertical-align:middle;font:bold 12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
td.c2 {width:10px;height:30px;vertical-align:middle;font:12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
@endsection

@section('content')
</table>                
</td>
</tr>
<tr>
    <td class="tc">
        <p class="sp">&nbsp;</p>
        <p class="ti1">PENDAFTARAN KAP DAN PIN SBMPTN 2017</p>
        <p class="sp">&nbsp;</p>
        <div class="row">
        	<div class="col-md-7"><p class="ti1">Memilih Cara</p></div>
        </div>
        <br>
        <form action="/cbt" method="post">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="container">
            <div class="row">
                <div class="col-md-11 col-md-offset-0">
                    <div class="panel panel-default">
                        <div class="panel-heading ">
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Rumpun</p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="rumpun" class="panlok" id="provinsi">
                                <option value="0" disabled="true" selected="true">-Select-</option>
                                <option>SAINTEK</option>
                                <option>SOSHUM</option>
                                <option>IPC</option>
                                </select> </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Pengerjaan</p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="cara" class="panlok" id="provinsi">
                                <option value="0" disabled="true" selected="true">-Select-</option>
                                <option>CBT</option>
                                <option>PBT</option>
                            </select> </div>
                            </div>
                              <div class="row ">
                                <div class="col-md-3"><p class="text-left">Tahun</p></div>
                                <div class="col-md-8 text-left"> 
                                 <input type="text" name="tahun">
                                 </div>
                            </div>
                      <div class="row ">
                                <div class="col-md-10"><p class="text-left"></p></div>
                                <div class="col-md-1 text-left">
                                <input class="btn btn-primary" type="submit" value="Simpan" /> 
                                </div>
                                </form>
                        </div>
                        <br>
                </div>
            </div>
        
        </div>
    <br /><br />
</td>
</tr>
</table>
@endsection