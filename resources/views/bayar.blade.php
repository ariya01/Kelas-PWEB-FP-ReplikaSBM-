@extends('layout.app')

@section('title')
Pendaftaran SBMPTN
@endsection

@section('css')
td.ty1 {width:100%;background-color:#e7efff;}
td.c1 {width:160px;height:30px;vertical-align:middle;font:bold 12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
td.c2 {width:10px;height:30px;vertical-align:middle;font:12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
@endsection

@section('content')
</table>                
</td>
</tr>
<tr>
    <td class="tc">
        <p class="sp">&nbsp;</p>
        <p class="ti1">PENDAFTARAN KAP DAN PIN SBMPTN 2017</p>
        <p class="sp">&nbsp;</p>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading ">
                            <div class="row ">
                                <div class="col-md-3"><img src="{{asset('img/logosbmptn.gif')}}" height="130" width="130"></div>
                                <div class="col-md-6"><font size="4" color="blue"><strong>SLIP PEMBAYARAN BIAYA SELEKSI</font><font color="blue" size="8"><h2>SBMPTN 2017</h2></font></strong></div>
                                <div class="col-md-3"><img src="{{asset('img/ristek.png')}}" height="130" width="130"></div>
                            </div>
                        </div>
                        

                        <div class="panel-body">
                            <div class="col-md-1"></div>
                            <div class="col-md-4"><font color="blue"><strong> KODE PEMBAYARAAN : </strong></font></div>
                            <div class="col-md-7"> <strong>{{$user->KAP}}</strong></div>
                        </div>
                        <div class="panel-body">
                            <div class="">Pembayaran harus dilakukan sebelum tanggal 02 - 06 - 2017 pukul 20.00 wib</div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div>
                            <p align="left">Kode Pembayaran ini berlaku bagi calon peserta data sebagai berikut :</p>
                            <table align="center" style="width: 800px">
                            <tr colspan="3">
                            <td></td>
                                <td></td>
                                <td></td>
                                <td rowspan="5">
                                        <img src="{{asset('img/sbm.png')}}" height="130" width="130">
                                    </td>
                            </tr>
                                <tr style="text-align: left">
                                    <td>KAP</td>
                                    <td>:</td>
                                    <td>{{$user->email}}</td>
                                    
                                </tr>
                                <tr style="text-align: left">
                                    <td>PIN</td>
                                    <td>:</td>
                                    <td>{{$user->password}}</td>
                                </tr>
                                <tr style="text-align: left">
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>{{$user->nama}}</td>
                                </tr>
                                <tr style="text-align: left">
                                    <td>Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>{{$user->lahir}}</td>

                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="panel-body" style="text-align: left">
                        <p>Pembayaran dapat dilakukan melalui :</p>
                        <ul>
                            <li>Bank Mandiri : ATM Mandiri, ATM Bersama, Internet Banking, SMS Banking, Teller di semua Cabang Bank Mandiri</li>
                            <li>Bank BNI : Teller di semua Cabang Bank Mandiri</li>
                        </ul>
                        <p align="center">Setelah melakukan pembayaran, anda harus login kembali untuk menyelesaikan proses pendaftaran SBMPTN 2015</p>
                        <br>
                        <p align="center">Apakah ada hal-hal yang kurang jelas tentang tata cara pembayaran biaya seleksi SBMPTN 2017, anda dapat melihatnya di <a href="">www.sbmptn.or.id</a>, atau menghubungi Help Desk SBMPTN 2017 di halo.sbmptn.or.id atau di nomor telepon 0804-1-456-456</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<br /><br />
</td>
</tr>
</table>
@endsection