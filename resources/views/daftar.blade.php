@extends('layout.app')

@section('title')
Pendaftaran SBMPTN
@endsection

@section('css')
            td.ty1 {width:100%;background-color:#e7efff;}
            td.c1 {width:160px;height:30px;vertical-align:middle;font:bold 12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
            td.c2 {width:10px;height:30px;vertical-align:middle;font:12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
@endsection

@section('content')
</table>                
    </td>
        </tr>
            <tr>
                <td class="tc">
                    <p class="sp">&nbsp;</p>
                    <p class="ti1">PENDAFTARAN KAP DAN PIN SBMPTN 2017</p>
                    <p class="sp">&nbsp;
</p>
                    <form action="/insert" method="post" >
                        <table class="ct">
                            <tr> 
                            {{ csrf_field()}}
                                <td class="ty1">
                                    <table width="100%">
                                        <tr>
                                          <td class="c1">Nama Lengkap</td>
                                          <td class="c2">:</td>
                                          <td style="text-align:left">
                                            <input name="nama" type="text">
                                            @if($errors->has('nama'))
                                            <p style="color:red;">Nama Belum diisi</p>
                                            @endif
                                            <br/>
                                            <span class="ket">Sesuai dengan yang tercantum di Ijazah</span>
                                          </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr> 
                                <td class="ty1">
                                    <table width="100%">
                                        <tr>
                                          <td class="c1">Tanggal Lahir</td>
                                          <td class="c2">:</td>
                                          <td style="text-align:left">
                                            <input name="lahir" type="date" />
                                            @if($errors->has('lahir'))
                                            <p style="color:red;">Tanggal Belum diisi</p>
                                            @endif
                                          </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="btcont" colspan="2">
                                    <input class="btn btn-primary" tabindex="8" type="submit" value="add" />
                                   
                                </td>
                            </tr> 
                        </table>
                    </form>
                    <br /><br />
                </td>
            </tr>
        </table>
@endsection