@extends('layout.app')

@section('title')
Pendaftaran SBMPTN
@endsection

@section('css')
td.ty1 {width:100%;background-color:#e7efff;}
td.c1 {width:160px;height:30px;vertical-align:middle;font:bold 12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
td.c2 {width:10px;height:30px;vertical-align:middle;font:12px Tahoma,Verdana,Arial,'Sans Serif';color:#2e72b6;text-align:left;}
@endsection

@section('content')
</table>                
</td>
</tr>
<tr>
    <td class="tc">
        <p class="sp">&nbsp;</p>
        <p class="ti1">PENDAFTARAN KAP DAN PIN SBMPTN 2017</p>
        <p class="sp">&nbsp;</p>
        <div class="row">
        	<div class="col-md-7"><p class="ti1">Pengisian Biodata</p></div>
        	<div class="col-md-4"><img src="{{ asset($hasil) }}" height="130" width="130"></div>
        </div>
        <br>
        <form action="/bio" method="post">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="container">
            <div class="row">
                <div class="col-md-11 col-md-offset-0">
                    <div class="panel panel-default">
                        <div class="panel-heading ">
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Nama Peserta sesuai yang terlulis dalam ijazah</p></div>
                                <div class="col-md-8 text-left">{{Auth::user()->nama}}</div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Nama yang akan dituls dilembar LJU</p></div>
                                <div class="col-md-8 text-left"><input type="text" name="input" size="20"><br>
                                 <span class="ket">Harus diisi,maksimal 20 huruf</span></div>
                            </div>
                             <div class="row ">
                                <div class="col-md-3"><p class="text-left">Nomor Induk Siswa Nasional</p></div>
                                <div class="col-md-8 text-left"><input type="text" name="induk" size="20"><br> <span class="ket">Harus diisi, 10 angka</span>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Alamat Tetap</p></div>
                                <div class="col-md-8 text-left"><input type="text" name="alamat" size="20"></div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Provinsi</p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="prov" class="provinsi" id="provinsi">
                                <option value="0" disabled="true" selected="true">-Select-</option>
                                @foreach ($prov as $a)
                                <option value="{{$a->kode}}">{{$a->namaprov}}</option>
                                @endforeach
                                </select> </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Kota</p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="subcategory" id="subcategory"> 
                                <option value="0" disabled="true" selected="true">Pilih Provinsi dulu</option>
                                <option value=""></option>
                                </select> </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Kode Pos</p></div>
                                <div class="col-md-8 text-left"> 
                                <input type="text" name="pos">
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Telepon/HP</p></div>
                                <div class="col-md-8 text-left"> 
                                <input type="text" name="hp">
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Email</p></div>
                                <div class="col-md-8 text-left"> 
                                <input type="text" name="surat">
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Konfirmasi Email</p></div>
                                <div class="col-md-8 text-left"> 
                                <input type="text" name="suratcek">
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Provinsi Tempat Lahir</p></div>
                                <div class="col-md-8 text-left"> 
                                <select class="provinsila" id="provinsila" name="provinsila">
                                <option value="0" disabled="true" selected="true">-Select-</option>
                                @foreach ($prov as $a)
                                <option value="{{$a->kode}}">{{$a->namaprov}}</option>
                                @endforeach
                                </select> </div>
                            </div>
                             <div class="row ">
                                <div class="col-md-3"><p class="text-left">Kota</p></div>
                                <div class="col-md-8 text-left"> 
                                <select name="subcategoryla" id="subcategoryla"> 
                                <option value="0" disabled="true" selected="true">Pilih Provinsi dulu</option>
                                <option value=""></option>
                                </select> </div>
                            </div>
                             <div class="row ">
                                <div class="col-md-3"><p class="text-left">Tanggal Lahir</p></div>
                                <div class="col-md-8 text-left"> 
                               	{{Auth::user()->lahir}}
                               	</div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Jenis Kelamin</p></div>
                                <div class="col-md-8 text-left"> 
                               <label class="radio-inline"><input type="radio" name="optradio" value="Laki-Laki">Laki - Laki</label>
								<label class="radio-inline"><input value="Perempuan" type="radio" name="optradio">Perempuan</label>
								</div>
                            </div>
                             <div class="row ">
                                <div class="col-md-3"><p class="text-left">Agama</p></div>
                                <div class="col-md-8 text-left"> 
								  	<select name="agama" id="sel1">
								    <option>Islam</option>
								    <option>Kristen</option>
								    <option>Katolik</option>
								    <option>Budha</option>
								    <option>Hindu</option>
								  </select>						
								 </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Kewarganegaraan</p></div>
                                <div class="col-md-8 text-left"> 
								  	<select name="warga" id="sel1">
								  	 <option value="0" disabled="true" selected="true">-Pilih-</option>
								    <option>WARGA NEGARA INDONESIA</option>
								    <option>WARGA NEGARA ASING</option>
								  </select>						
								 </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Jumlah Saudara</p></div>
                                <div class="col-md-1 text-left"> 
								  	<select name="jumadik" id="sel1">
								  	 <option value="0" disabled="true" selected="true">-Adik-</option>
								    <option>1</option>
								    <option>2</option>
								    <option>3</option>
								    <option>4</option>
								  </select>						
								 </div>
								  <div class="col-md-1 text-left"> 
								  	<select name="jumkakak" id="sel1">
								  	 <option value="0" disabled="true" selected="true">-Kakak-</option>
								    <option>1</option>
								    <option>2</option>
								    <option>3</option>
								    <option>4</option>
								  </select>						
								 </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Nama Ayah/ WALI</p></div>
                                <div class="col-md-8 text-left"> 
                                <input type="text" name="namaayah">
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Nama Ibu</p></div>
                                <div class="col-md-8 text-left"> 
                                <input type="text" name="namaibu">
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Pendidikan Orang Tua/ Wali</p></div>
                                <div class="col-md-2 text-left"> 
								  	<select name="penayah" id="sel1">
								  	 <option value="0" disabled="true" selected="true">-Ayah / Wali-</option>
								    <option>Tidak Tamat SD</option>
								    <option>SD</option>
								    <option>SMP</option>
								    <option>SMA</option>
								    <option>D1</option>
								    <option>D2</option>
								    <option>D3</option>
								    <option>D4</option>
								    <option>Sarjana</option>
								    <option>Magister</option>
								    <option>Doktor</option>
								  </select>						
								 </div>
								  <div class="col-md-1 text-left"> 
								  	<select name="penibu" id="sel1">
								  	 <option value="0" disabled="true" selected="true">-Ibu-</option>
								    <option>Tidak Tamat SD</option>
								    <option>SD</option>
								    <option>SMP</option>
								    <option>SMA</option>
								    <option>D1</option>
								    <option>D2</option>
								    <option>D3</option>
								    <option>D4</option>
								    <option>Sarjana</option>
								    <option>Magister</option>
								    <option>Doktor</option>
								  </select>						
								 </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Pekerjaan Orang Tua/ Wali</p></div>
                                <div class="col-md-2 text-left"> 
								  	<select name="pekayah" id="sel1">
								  	 <option value="0" disabled="true" selected="true">-Ayah / Wali-</option>
								    <option>Tidak Tamat SD</option>
								    <option>TNI</option>
								    <option>Petani</option>
								    <option>Guru</option>
								    <option>PNS</option>
								  </select>						
								  	<select name="pekibu" id="sel1">
								  	 <option value="0" disabled="true" selected="true">-Ibu-</option>
								    <option>Tidak Tamat SD</option>
								    <option>TNI</option>
								    <option>Petani</option>
								    <option>Guru</option>
								    <option>PNS</option>
								  </select>						
								 </div>
                            </div>
                            <br>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Penghasilan Total Orang Tua Wali</p></div>
                                <div class="col-md-2 text-left"> 
								  	<select name="hasayah" id="sel1">
								  	 <option value="0" disabled="true" selected="true">-Ayah / Wali-</option>
								    <option>Rp 1.000.000,00</option>
								    <option>Rp 2.000.000,00</option>
								    <option>Rp 3.000.000,00</option>
								    <option>Rp 4.000.000,00</option>
								    <option>Rp 5.000.000,00</option>
								  </select>						
								  	<select name="hasibu" id="sel1">
								  	 <option value="0" disabled="true" selected="true">-Ibu-</option>
								     <option>Rp 1.000.000,00</option>
								    <option>Rp 2.000.000,00</option>
								    <option>Rp 3.000.000,00</option>
								    <option>Rp 4.000.000,00</option>
								    <option>Rp 5.000.000,00</option>
								  </select>						
								 </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3"><p class="text-left">Kebutuhan Khusus</p></div>
                                <div class="col-md-2 text-left"> 
								  	<div class="checkbox">
									  <label><input type="checkbox" name="but" value="TUNA NETRA">TUNA NETRA</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" name="but" value="TUNA RUNGU">TUNA RUNGU</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" name="but" value="TUNA WICARA">TUNA WICARA</label>
									</div>
									<div class="checkbox">
									  <label><input type="checkbox" name="but" value="TUNA DAKSA">TUNA DAKSA</label>
									</div>
								</div>
                            </div>
                        </div>
                        <br>
                        <div class="row ">
                                <div class="col-md-10"><p class="text-left"></p></div>
                                <div class="col-md-1 text-left">
                                <input class="btn btn-primary" type="submit" value="Simpan" /> 
                                </div>
                                </form>
                        </div>
                        <br>
                        <form action="/keluar_kok" method="post">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}"> 
                        <div class="row ">
                                <div class="col-md-10"><p class="text-left"></p></div>
                                <div class="col-md-1 text-left">
                                <input class="btn btn-primary" type="submit" value="Keluar" /> 
                                </div>
                        </div>
                        </form>
                        <br>
                    </div>
                </div>
            </div>
        
        </div>
    <br /><br />
</td>
</tr>
</table>
<<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
        $('#provinsi').on('change',function(e){
            // console.log(e);

            var cat_id =e.target.value;
            // console.log(cat_id);
        	$.get('/ajaxku/'+cat_id, function(data){
        	// console.log(data);
        	$('#subcategory').empty();
        	$.each(data,function(index,subcat){
        		$('#subcategory').append('<option value="'+subcat.id+'">'+subcat.namakota+'</option>');
        	});
        })
	});

$('#provinsila').on('change',function(e){
            // console.log(e);

            var cat_id =e.target.value;
            // console.log(cat_id);
        	$.get('/ajaxku/'+cat_id, function(data){
        	// console.log(data);
        	$('#subcategoryla').empty();
        	$.each(data,function(index,subcat){
        		$('#subcategoryla').append('<option value="'+subcat.id+'">'+subcat.namakota+'</option>');
        	});
        })
	});
</script>
@endsection