<!DOCTYPE html>  
<html dir="ltr" lang="id">
    <head>
        <title>@yield('title')</title>
            <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="31 Dec 2017 00:00:00 GMT" /> 
        <meta http-equiv="x-frame-options" content="deny" />
        <link rel="stylesheet" type="text/css" href="sbmptn.css" /> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <link rel="icon" href="{{'img/logosbmptn.gif'}}" />
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/sbmptn.css')}}" />
         <link href="{{URL::asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
 
        <style type="text/css">

            @yield('css');
            
        </style>
        <script type="text/javascript" charset="UTF-8">
        <!-- 
            if(!navigator.cookieEnabled) {
                var t="./wcookie.php";
                window.location.assign(t);
            } 
        -->
        </script>
    </head>
    <body>
        <table class="st1">
            <tr>
                <td class="tc">
                    
<table class="st1">
    <tr class="nb">
    
        <td style="text-align:center;border:0;border-spacing:0;padding:0;" colspan="3">
            <table class="st1">
                <tr>
                    <td class="logo1">
                        <a href="#">
                            <img src="{{asset('img/logosbmptn.gif')}}" alt="Logo SBMPTN" />
                        </a>
                    </td>
                    <td class="logo2">
                        <span class="f1">Laman Resmi</span><br />
                        <span class="f2" style="color:white;">SBMPTN</span>
                        <span class="f2" style="color:#ff871b;">2017</span><br />
                        <span class="f1">
                            Seleksi Bersama Masuk Perguruan Tinggi Negeri Tahun 2017
                        </span>
                    </td>
                    <td class="logo3">
                        <img style="vertical-align:middle" src="{{asset('img/bank.gif')}}" alt="Logo Bank Mitra" />
                        <img style="vertical-align:middle" src="{{asset('img/telkom.gif')}}" alt="Logo Telkom" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="background-color:#bbbbbb;height:3px">
        </td>
    </tr>
    <tr>
        <td class="marquee">
            <span>PANLOK 11 menyelenggarakan ujian keterampilan Olah Raga, Seni Rupa, Sendratasik, Seni Tari, dan Seni Musik di Universitas Syiah Kuala</span>
        </td>
    </tr>
    @yield('content');
    <table style="text-align:center;">
                        <tr>
                            <td align="left">
                                <span class="attti"><strong>PERHATIAN :</strong></span>
                                <ul style="margin-top: 0px; margin-bottom: 0px; list-style-type:square">
                                    <li class="att">Bagi peserta Bidikmisi, KAP dan PIN yang dimasukkan harus sesuai dengan yang tercetak di Kartu Peserta Bidikmisi.</li>
                                    <li class="attsp">Jika meninggalkan komputer atau akan dipakai oleh pengguna lain, 
                                        pastikan Anda sudah keluar dengan cara menekan tombol <b>"Keluar"</b> yang tersedia </li>
                                    <li class="att">Apabila ada hal-hal yang kurang jelas tentang pendaftaran online ini, 
                                        dapat menghubungi Help Desk SBMPTN 2017 di alamat <span class="attbd"><b>http://halo.sbmptn.ac.id</b></span> 
                                        atau nomor telepon <span class="attbd"><b>0804-1-456-456</b></span></li>
                                </ul>
                                <br />
                                <img src="./images/digicert.jpg" alt="Logo DigiCert" />
                            </td>
                        </tr>
                    </table>
    </body>
</html>
