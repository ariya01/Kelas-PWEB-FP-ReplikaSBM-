<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('bios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('nama');
            $table->string('input');
            $table->string('induk');
            $table->string('alamat');
            $table->string('kodeprov');
            $table->string('kodekot');
            $table->string('pos');
            $table->string('hp');
            $table->string('surat');
            $table->string('kodesurat');
            $table->string('kodeprovla');
            $table->string('kodekotla');
            $table->date('lahir');
            $table->string('jenis');
            $table->string('agama');
            $table->string('warga');
            $table->integer('jumadik');
            $table->integer('jumkakak');
            $table->string('namaayah');
            $table->string('namaibu');
            $table->string('penayah');
            $table->string('penibu');
            $table->string('pekayah');
            $table->string('pekibu');
            $table->string('hasayah');
            $table->string('hasibu');
            $table->string('but');
            $table->timestamps();

              $table->foreign('email')
             ->references('email')->on('pins')
             ->onDelete('cascade')->onUpdate('cascade');
             
             //  $table->foreign('kodeprov')
             // ->references('kode')->on('propinsis')
             // ->onDelete('cascade')->onUpdate('cascade');
            
            // $table->foreign('kodekot')
            // ->references('id')->on('kotas')
            // ->onDelete('cascade')->onUpdate('cascade');
            
            //  $table->foreign('kodeprovla')
            // ->references('kode')->on('propinsis')
            // ->onDelete('cascade')->onUpdate('cascade');
            
            // $table->foreign('kodekotla')
            // ->references('id')->on('kotas')
            // ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
