<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCbtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cbts', function (Blueprint $table) {
            $table->string('kode');
            $table->primary('kode');
            $table->string('email')->unique();
            $table->string('cara');
            $table->string('rumpun');
            $table->string('tahun');
            $table->timestamps();

             $table->foreign('email')
             ->references('email')->on('pins')
             ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
