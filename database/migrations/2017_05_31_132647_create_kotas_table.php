<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('kotas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kodeprov');
            $table->string('namakota');
            $table->timestamps();

            $table->foreign('kodeprov')
            ->references('kode')->on('provinsis')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::dropIfExists('kotas'); 
    }
}
