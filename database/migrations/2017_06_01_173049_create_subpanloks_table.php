<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubpanloksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('subpanlokss', function (Blueprint $table) {
            $table->string('kodesub');
            $table->primary('kodesub');
            $table->string('kodepanlok');
            $table->string('namasub');
            $table->timestamps();
            
             $table->foreign('kodepanlok')
             ->references('kodepanlok')->on('panloks')
             ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
