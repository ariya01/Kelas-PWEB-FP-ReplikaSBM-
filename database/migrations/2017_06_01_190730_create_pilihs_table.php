<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePilihsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('pilihs', function (Blueprint $table) {
            $table->string('kode');
            $table->primary('kode');
            $table->string('email')->unique();
            $table->string('panlok');
            $table->string('subpanlok');
            $table->string('univ1');
            $table->string('jur1');
            $table->string('univ2');
            $table->string('jur2');
            $table->string('univ3');
            $table->string('jur3');
            $table->string('olah');
            $table->string('seni');
            $table->string('sendra');
            $table->timestamps();

             $table->foreign('email')
             ->references('email')->on('pins')
             ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
