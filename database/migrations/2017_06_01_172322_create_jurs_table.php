<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('jurs', function (Blueprint $table) {
            $table->string('kodejur');
            $table->primary('kodejur');
            $table->string('kodeuniv');
            $table->string('namauniv');
            $table->timestamps();
            
             $table->foreign('kodeuniv')
             ->references('kodeuniv')->on('univs')
             ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::dropIfExists('jurs');
    }
}
